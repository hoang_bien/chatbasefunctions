'use strict'
const functions = require('firebase-functions');


const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);


exports.addMemberToGroup = functions.database.ref('/users/{userId}/groups/{groupId}').onCreate(event => {
    return updateUserToGroup(event.params.userId, event.params.groupId);
});

exports.acceptInviteRequest = functions.database.ref('invites/{userId}/{inviteId}').onDelete(event => {
    const userId = event.params.userId;
    const inviteId = event.params.inviteId;
    const messageUid = event.data.uid;
    return admin.database().ref(`userChats/${inviteId}/${userId}`).once('value').then(function (snapshot) {
        const messageToAdd = [];
        snapshot.forEach(function (messageSnapshot) {
            let message = messageSnapshot.val();
            if (message.uid !== messageUid) {
                message.unread = true;
            }
            messageToAdd.push(admin.database().ref(`userChats/${userId}/${inviteId}`).child(message.uid).set(message));
        });
        return Promise.all(messageToAdd);
    });

});

exports.saveLastMessageForGroup = functions.database.ref('groupMessages/{groupId}/{messageId}').onCreate(event => {
    const message = event.data.val();
    const sender = message.sender;
    if (message.content == null) {
        const data = {
            messageType: message.messageType,
            sender: message.sender,
            timestamp: message.timestamp,
            readBy:{}
          };
        data.readBy[sender] = true;
        return admin.database().ref('groups').child(event.params.groupId).update(data);
    } else {
        const data = {
            lastMessage: message.content,
            messageType: message.messageType,
            sender: message.sender,
            timestamp: message.timestamp,
            readBy:{}
        };
        data.readBy[sender] = true;
        return admin.database().ref('groups').child(event.params.groupId).update(data);
    }

});

exports.sendUserNotification = functions.database.ref('userChats/{receiverId}/{senderId}/{messageId}').onCreate(event => {
    const msg = event.data.val();
    const receiverId = event.params.receiverId;
    const messageId = event.params.messageId;
    console.log('New message: ' + msg);
    if (msg.unread) {
        const getTokenPromise = admin.database().ref("fcm").child(receiverId).once('value');
        const getUserInfoPromise = admin.database().ref("users").child(receiverId).once('value');
        Promise.all([getTokenPromise, getUserInfoPromise]).then(results => {

            const tokensSnapshot = results[0];
            const user = results[1].val();
            console.log(tokensSnapshot + " " + user);
            if (tokensSnapshot.exists()) {
                const payload = {
                    data: {
                        name: user.name,
                        uid: messageId,
                        notificationId: msg.sender,
                        notificationType: "user_message",
                        content: msg.content,
                        messageType: msg.messageType,
                        timestamp: msg.timestamp.toString(),
                        sender: msg.sender,
                        senderName: user.name,
                        senderProfileImage: user.profileImage
                    }
                };
                return admin.messaging().sendToDevice(tokensSnapshot.val(), payload).then(response => {
                    console.log("Successfully sent message:", response);
                }).catch(error => {
                    console.log("Error sending message:", error);
                    if (error.code === 'messaging/invalid-registration-token' ||
                        error.code === 'messaging/registration-token-not-registered') {
                        return tokensSnapshot.ref.child(tokens[index]).remove();
                    }
                });
            }
        });

    }
});

exports.sendGroupNotification = functions.database.ref("groupMessages/{groupId}/{messageId}").onCreate(event => {
    const groupId = event.params.groupId;
    const msg = event.data.val();
    const senderId = msg.sender;
    const messageId = event.params.messageId;

    const getSenderInfoPromise = admin.database().ref(`users/${senderId}`).once('value');
    const getGroupNamePromise = admin.database().ref(`groups/${groupId}/title`).once('value');

    const getTokensSnapshotPromise = admin.database().ref(`groups/${groupId}/users`).once('value').then(snapshot => {
        if (snapshot.val === null) {
            return Promise.reject(new Error("Couldn't get users in the group"));
        }
        const userIds = Object.keys(snapshot.val());
        userIds.splice(userIds.indexOf(senderId), 1);
        const getUserTokensPromise = userIds.map(token => admin.database().ref(`fcm/${token}`).once('value'));
        return Promise.all(getUserTokensPromise);
    });

    return Promise.all([getSenderInfoPromise, getTokensSnapshotPromise, getGroupNamePromise]).then(results => {
        const user = results[0].val();
        const tokensSnapshot = results[1];
        const tokens = [];
        for (let i = 0; i < tokensSnapshot.length; i++) {
            const val = tokensSnapshot[i].val();
            if (val !== null) {
                tokens.push(val);
            }
        }
        if (tokens.length > 0) {
            const payload = {
                data: {
                    notificationType: "group_message",
                    notificationId: groupId,
                    name: results[2].val(),
                    uid: messageId,
                    content: msg.content,
                    messageType: msg.messageType,
                    timestamp: msg.timestamp.toString(),
                    sender: msg.sender,
                    senderName: user.name
                }
            };
            return admin.messaging().sendToDevice(tokens, payload).then(response => {
                console.log("Successfully sent to users" + JSON.stringify(tokens) + " with message:" + JSON.stringify(payload));
            });
        }
    });
});

exports.sendInviteNotification = functions.database.ref("invites/{receiverId}/{senderId}").onCreate(event => {
    const receiverId = event.params.receiverId;
    const senderId = event.params.senderId;
    const getSenderInfoPromise = admin.database().ref(`users/${senderId}`).once('value');
    const getTokenPromise = admin.database().ref(`fcm/${receiverId}`).once('value');
    return Promise.all([getSenderInfoPromise, getTokenPromise]).then(results => {
        const user = results[0].val();
        const token = results[1].val();
        if (token !== null) {
            const payload = {
                data: {
                    notificationId:senderId,
                    notificationType: "invite_message",
                    name: user.name,
                    sender: user.uid,
                    senderName: user.name,
                    senderProfileImage:user.profileImage
                }
            };
            return admin.messaging().sendToDevice(token, payload).then(response => {
                console.log("Sent successfully message " + JSON.stringify(payload));
            });
        }
    })
});


function updateUserToGroup(userId, groupId) {
    return admin.database().ref('users').child(userId).once('value').then(function (userSnapshot) {
        const user = userSnapshot.val();
        const userName = user.name;
        const userImage = user.profileImage;
        return admin.database().ref('groups').child(groupId).child('users').child(userId).update({
            name: userName,
            profileImage: userImage,
            uid: userId
        });
    });
}
